#   HOW MUCH TIME DOES IT TAKE TO LEARN A NEW SKILL
![](/uploads/3692671f7ad0d6ab9a04c7bcfe549341/20hours.jpg)

## DECONSTRUCT THE SKILL

![](/uploads/7098294331dfb399fe055088e6b90cbc/break.jpg)
Break the skills into smaller parts and learn the most important thing first

## LEARN ENOUGH TO SELF-CORRECT

![](/uploads/4985dd192a44679ae5ea5cf70a4cff3d/procrastinate.png)

- Dont find a way to procastinate 
- Just start on it after getting a little knowledge

## REMOVE PRACTICE BARRIERS
![stayfocus](/uploads/879d418cf35025a9235ce303c17fdbc5/stayfocus.jpg)
STAY FOCUSED

## PRACTICE AT LEAST " 20 HOURS "

![practice](/uploads/dbe6e26e333c7e98949c97f93dc2778e/practice.jpeg)