![head](/uploads/ce940c626a31c3a7a39291b98cc54ddf/head.jpg)

![rasp](/uploads/ff455b1492d973259319ca22365619ae/rasp.jpeg)

## Operates in the open source ecosystem
### Low cost ,credit card sized computer
 Used to :
- learn programming skills 
- build hardware project
- do home automation                        
![homea](/uploads/94998b3a6477393265352c603e3a1a18/homea.jpg)

- Key to the Pi's success is it's low price
- Languages like Python ,C,C++, Java , Scratch and Ruby are installed by default
